### Đối với dự án áp dụng quy định tương ứng với 1 pull-request chỉ cho phép 1 commit

Từ đây, Central Repository và Forked Repository sẽ được gọi lần lượt là `upstream` và `origin`.

0. Tạo các workpace riêng `upstream` cho dự án chính và `origin` cho dự án fork theo các lệnh `remote`
    ```
    git remote add upstream ....link_repo......
    git remote -v
    ```

1. Đồng bộ hóa branch develop tại local với upstream.
    ```sh
    $ git checkout develop
    $ git pull upstream develop

    //  trong trường hợp làm việc với nhánh `testXXX` không có nhánh `testXXX` trên local thì gõ bằng câu lệnh dưới rồi quay lại làm như bình thường
    $ git fetch upstream  
    ```

2. Tạo branch để làm task từ branch develop ở local. Tên branch là số ticket của task（Ví dụ: `task/1234`）.
    ```sh
    $ git checkout develop # <--- Không cần thiết nếu đang ở trên branch develop
    $ git checkout -b task/1234
    ```

3. Tiến hành làm task（Có thể commit bao nhiêu tùy ý）.

4. Trường hợp đã tạo nhiều commit trong quá trình làm task、tại 5. trước khi push phải dùng rebase -i để hợp các commit lại thành 1 commit duy nhất.
    ```sh
    $ git rebase develop -i
    ```

5. Quay trở về branch develop ở local và lấy code mới nhất về

    ```sh
    $ git checkout develop
    $ git pull upstream develop
    ```

6. Quay trở lại branch làm task, sau đó rebase với branch develop.

    ```sh
    $ git checkout task/1234
    $ git rebase develop
    ```
    **Trường hợp xảy ra conflict trong quá trình rebase、hãy thực hiện các thao tác của mục「Khi xảy ra conflict trong quá trình rebase」.**

7. Push code lên origin.

    ```sh
    $ git push origin task/1234
    ```

8. Tại origin trên Github（Bitbucket）、từ branch `task/1234` đã được push lên hãy gửi pull-request đối với branch develop của upstream.

9. Hãy gửi link URL của trang pull-request cho reviewer trên chatwork để tiến hành review code.

    9.1. Trong trường hợp reviewer có yêu cầu sửa chữa, hãy thực hiện các bước 3. 〜 6.

    9.2 push -f (push đè hoàn toàn lên code cũ) đối với remote branch làm task.
    ```sh
    $ git push origin task/1234 -f
    ```

    9.3 Tiếp tục gửi lại URL cho reviewer trên chatwork để tiến hành việc review code.

10. Nếu trên 2 người reviewer đồng ý với pull-request, người reviewer cuối cùng sẽ thực hiện việc merge pull-request.
    Revewer xác nhận sự đồng ý bằng comment LGTM.

11. Quay trở lại 1.

#### Khi xảy ra conflict trong quá trình rebase

Khi xảy ra conflict trong quá trình rebase, sẽ có hiển thị như dưới đây (tại thời điểm này sẽ bị tự động chuyển về một branch vô danh)
```sh
$ git rebase develop
First, rewinding head to replay your work on top of it...
Applying: refs #1234 Sửa lỗi cache
Using index info to reconstruct a base tree...
Falling back to patching base and 3-way merge...
Auto-merging path/to/conflicting/file
CONFLICT (add/add): Merge conflict in path/to/conflicting/file
Failed to merge in the changes.
Patch failed at 0001 refs #1234 Sửa lỗi cache
The copy of the patch that failed is found in:
    /path/to/working/dir/.git/rebase-apply/patch

When you have resolved this problem, run "git rebase --continue".
If you prefer to skip this patch, run "git rebase --skip" instead.
To check out the original branch and stop rebasing, run "git rebase --abort".
```

1. Hãy thực hiện fix lỗi conflict thủ công（những phần được bao bởi <<< và >>> ）.
Trong trường hợp muốn dừng việc rebase lại, hãy dùng lệnh `git rebase --abort`.

2. Khi đã giải quyết được tất cả các conflict, tiếp tục thực hiện việc rebase bằng:

    ```sh
    $ git add .
    $ git rebase --continue
    ```
